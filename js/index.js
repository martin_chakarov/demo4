$(document).ready(function () {

    let videoSourcesObj = [

        {
            videoId: 554288213,
            video: "https://player.vimeo.com/video/554288213?h=0f0f3a0b54&title=0&byline=0&portrait=0",
            videoName: "Ankles and Feet"
        },
        {
            videoId: 554288497,
            video: "https://player.vimeo.com/video/554288497?h=be10d13447&title=0&byline=0&portrait=0",
            videoName: "Arms and Hands"
        },
        {
            videoId: 554288640,
            video: "https://player.vimeo.com/video/554288640?h=88145b3198&title=0&byline=0&portrait=0",
            videoName: "Eyes"
        },
        {
            videoId: 554288734,
            video: "https://player.vimeo.com/video/554288734?h=a8f315aeee&title=0&byline=0&portrait=0",
            videoName: "Fingers"
        },
        {
            videoId: 554288843,
            video: "https://player.vimeo.com/video/554288843?h=0da54f5d84&title=0&byline=0&portrait=0",
            videoName: "Lip Puckering"
        },
        {
            videoId: 554289117,
            video: "https://player.vimeo.com/video/554289117?h=5f0e6734ec&title=0&byline=0&portrait=0",
            videoName: "Mouth"
        }
    ];

    let iframe = $('.active-video')[0];
    let player = new Vimeo.Player(iframe);

    let $videos = $('.video');
    let $dropdowns = $('.dropdown');
    let $mainMenu = $('.main-menu');
    let activeVideoIndex = 0;

    player.on('ended', function () {
        automaticPlay();
    })


    $videos.click(function (e) {
        changeVideo(e);
    })

    $dropdowns.mouseover(function (e) {
        hoverDropdown(e);
    })

    $mainMenu.mouseleave(function () {
        let $activeOption = $('.option.active');
        let $activeOptionMenu = $activeOption.parent().parent();

        if (!$activeOptionMenu.hasClass('display')) {

            let $displayedMenu = $('.display');

            toggleandUntoggleClasses($activeOptionMenu, $displayedMenu);
        }
    });

    function automaticPlay() {
        let nextVideoIndex = activeVideoIndex + 1;
        let domActiveVideo;
        let indexOfDomActiveVideo;

        player.loadVideo(videoSourcesObj[nextVideoIndex].videoId).then(function () {

            player.play();
        });

        $videos.each(function (index, value) {

            if (value.classList.contains('active')) {
                domActiveVideo = value;
                indexOfDomActiveVideo = index;
            };
        })

        let domNextVideo = $videos[indexOfDomActiveVideo + 1];

        $(domActiveVideo).removeClass('active');
        $(domNextVideo).addClass('active');

        activeVideoIndex = activeVideoIndex + 1;
    }

    function changeVideo(e) {
        let activeVideo;
        let clickedVideo;

        $videos.each(function (index, value) {

            if (value.classList.contains('active')) {
                activeVideo = value;
            };
        })

        if (e.target.className === 'video' || e.target.classList.contains('video')) {
            clickedVideo = e.target;
        }
        else {
            clickedVideo = e.target.parentElement;
        }

        let clickedVideoName = $(clickedVideo).find('.video__name').text().trim();
        let indexOfClickedVideo = videoSourcesObj.indexOf(videoSourcesObj.find((video) => video.videoName === clickedVideoName));


        player.loadVideo(videoSourcesObj[indexOfClickedVideo].videoId).then(function () {
            player.play();
        });

        activeVideoIndex = indexOfClickedVideo;

        $(activeVideo).removeClass('active');
        $(clickedVideo).addClass('active');
    }

    function hoverDropdown(e) {
        let hoveredDropdown;

        if (e.target.className == 'dropdown' || e.target.classList.contains('dropdown')) {
            hoveredDropdown = e.target;
        }
        else if (e.target.classList.contains('fas')) {
            hoveredDropdown = e.target.parentElement.parentElement;
        }
        else {
            hoveredDropdown = e.target.parentElement;
        }

        let classToDisplay = $(hoveredDropdown).text().trim().split(' ')[0].toLocaleLowerCase().trim();


        let $activeElement = $('.display');
        let $hoveredElement = $(`.${classToDisplay}`);

        toggleandUntoggleClasses($hoveredElement, $activeElement);
    }

    function toggleandUntoggleClasses(elementToDisplay, elementToHide) {

        let elementToDisplayTextContent = elementToDisplay.children()[0];
        let elementToHideTextContent = elementToHide.children()[0];

        elementToHide.removeClass('display');
        elementToHideTextContent.classList.add('text-none');
        elementToHide.addClass('none');

        elementToDisplayTextContent.classList.remove('text-none');
        elementToDisplay.removeClass('none');
        elementToDisplay.addClass('display');
    }

})